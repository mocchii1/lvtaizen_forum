#!/usr/bin/perl

#┌─────────────────────────────────
#│ Web Forum : check.cgi - 2013/01/13
#│ Copyright (c) KentWeb
#│ http://www.kent-web.com/
#└─────────────────────────────────

# モジュール宣言
use strict;
use CGI::Carp qw(fatalsToBrowser);

# 外部ファイル取込み
require './init.cgi';
my %cf = &init;

print <<EOM;
Content-type: text/html; charset=utf-8

<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>Check Mode</title>
</head>
<body>
<b>Check Mode: [ $cf{version} ]</b>
<ul>
EOM

# データファイル
my %log = (logfile => 'ログファイル', sesfile => 'セッションファイル');
foreach ( keys %log ) {
	if (-f $cf{$_}) {
		print "<li>$log{$_}パス : OK\n";

		if (-r $cf{$_} && -w $cf{$_}) {
			print "<li>$log{$_}パーミッション : OK\n";
		} else {
			print "<li>$log{$_}パーミッション : NG\n";
		}
	} else {
		print "<li>$log{$_}のパス : NG\n";
	}
}

# 過去ログ
print "<li>過去ログ：";
if ($cf{pastkey} == 0) {
	print "設定なし\n";
} else {
	print "設定あり\n";

	# NOファイル
	if (-e $cf{nofile}) {
		print "<li>過去ログNOファイルパス : OK\n";
		if (-r $cf{nofile} && -w $cf{nofile}) {
			print "<li>過去ログNOファイルパーミッション : OK\n";
		} else {
			print "<li>過去ログNOファイルパーミッション : NG\n"; }
	} else {
			print "<li>過去ログNOファイルパス : NG\n";
	}

	# ディレクトリ
	if (-d $cf{pastdir}) {
		print "<li>過去ログディレクトリパス : OK\n";
		if (-r $cf{pastdir} && -w $cf{pastdir} && -x $cf{pastdir}) {
			print "<li>過去ログディレクトリパーミッション : OK\n";
		} else {
			print "<li>過去ログディレクトリパーミッション : NG\n";
		}
	} else { print "<li>過去ログディレクトリパス : NG\n"; }
}

# 画像ディレクトリ
if (-d $cf{upldir}) {
	print "<li>画像ディレクトリパス : OK\n";

	if (-r $cf{upldir} && -w $cf{upldir} && -x $cf{upldir}) {
		print "<li>画像ディレクトリパーミッション : OK\n";
	} else {
		print "<li>画像ディレクトリパーミッション : NG\n";
	}
} else {
	print "<li>画像ディレクトリパス : NG\n";
}

# メール設定
print "<li>sendmailパス : ";
if (-e $cf{sendmail}) {
	print "OK\n";
} else {
	print "NG\n";
}

# テンプレート
my @tmpl = qw|conf edit error find finish list message note past read relate enter|;
foreach (@tmpl) {
	if (-e "$cf{tmpldir}/$_.html") {
		print "<li>テンプレート( $_.html ) : OK\n";
	} else {
		print "<li>テンプレート( $_.html ) : NG\n";
	}
}

# Image-Magick動作確認
eval { require Image::Magick; };
if ($@) {
	print "<li>Image-Magick動作: NG\n";
} else {
	print "<li>Image-Magick動作: OK\n";
}

print <<EOM;
</ul>
</body>
</html>
EOM
exit;

