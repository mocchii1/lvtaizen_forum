#!/usr/bin/perl

#┌─────────────────────────────────
#│ Web Forum : regist.cgi - 2013/05/31
#│ Copyright (c) KentWeb
#│ http://www.kent-web.com/
#└─────────────────────────────────

# モジュール宣言
use strict;
use CGI::Carp qw(fatalsToBrowser);
use lib "./lib";
use CGI::Minimal;
use Jcode;
use Encode 'decode';
# 設定ファイル認識
require "./init.cgi";
my %cf = init();

# データ受理
CGI::Minimal::max_read_size($cf{maxdata});
my $cgi = CGI::Minimal->new;
error('容量オーバー') if ($cgi->truncated);
my %in = &parse_form($cgi);

# アクセス制限
passwd(%in) if ($cf{enter_pwd} ne '');

# useragent取得
my $ua = $ENV{"HTTP_USER_AGENT"};

# 処理分岐
if ($in{mode} eq "regist") { regist(); }
elsif ($in{mode} eq "user_edit") { user_edit(); }
elsif ($in{mode} eq "user_dele") { user_dele(); }
error('不明な処理です');

#-----------------------------------------------------------
#  書込処理
#-----------------------------------------------------------
sub regist {
	# POSTメソッド以外は不受理
	if ($ENV{REQUEST_METHOD} ne 'POST') { error("不正なリクエストです"); }

	# 入力チェック
	check_input();

	# 投稿キーチェック
	if ($cf{use_captcha} > 0) {
		require $cf{captcha_pl};
		if ($in{captcha} !~ /^\d{$cf{cap_len}}$/) {
			open(FH,">> ./er.dat") or error("open err: er.dat");
			print FH "阻止：$in{name} $in{sub} $in{message} $in{captcha} \x0A\r\n";
			close(FH);
			error("投稿キーが入力不備です。<br>投稿フォームに戻って再読込み後、再入力してください");

		}

		# 投稿キーチェック
		# -1 : キー不一致
		#  0 : 制限時間オーバー
		#  1 : キー一致
		my $chk = cap::check($in{captcha},$in{str_crypt},$cf{captcha_key},$cf{cap_time},$cf{cap_len});
		if ($chk == 0) {
			error("投稿キーが制限時間を超過しました。<br>投稿フォームに戻って再読込み後、指定の数字を再入力してください");
		} elsif ($chk == -1) {
			open(FH,">> ./er.dat") or error("open err: er.dat");
			print FH "阻止：$in{name} $in{sub} $in{message} $in{captcha} \x0A\r\n";
			close(FH);
			error("投稿キーが不正です。<br>投稿フォームに戻って再読込み後、再入力してください");

		}
	}

	# ホストチェック
	my ($host,$addr) = check_host();

	# ログファイル読み込み
	open(DAT,"+< $cf{logfile}") or error("open err: $cf{logfile}");
	eval "flock(DAT, 2);";
	my @lines = <DAT>;

	# カウントファイルをアップ
	my $top = shift(@lines);
	my ($count,$ip,$tim) = split(/<>/,$top);
	$count = $count % 9999 ? ++$count : 1;

	# 時間を取得
	my $times = time;
	my $date = get_time($times,"log");

	# 二重投稿の禁止
	my $flg;
	if ($cf{regCtl} == 1) {
		if ($addr eq $ip && $times - $tim < $cf{wait}) { $flg++; }
	} elsif ($cf{regCtl} == 2) {
		if ($times - $tim < $cf{wait}) { $flg++; }
	}
	if ($flg) {
		close(DAT);
		error("現在投稿制限中です。もうしばらくたってから投稿をお願いします");
	}

	my $flg;
	foreach (@lines) {
		my ($name,$msg) = (split(/<>/))[6,8];
		if ($in{name} eq $name && $in{message} eq $msg) { $flg++; last; }
	}
	if ($flg) { error("二重投稿は禁止です"); }

	# 画像アップ
	my ($ext,$w,$h) = upload($count) if ($in{upfile});

	# クッキー格納
	set_cookie($in{name},$in{email},$in{url},$in{smail});

	# パスワード暗号化
	my $ango = encrypt($in{pwd}) if ($in{pwd} ne "");

	## --- 親記事の場合
	my (@new,@tmp);
	if ($in{no} eq 'new') {
		unshift (@lines,"$count<>no<>0<>$in{sub}<>$in{email}<>$in{url}<>$in{name}<>$date<>$in{message}<>$times<>$host<>$ango<>$in{wrap}<>$count<>$in{smail}<>0<>$ext<>$w<>$h<>\n");
		@new = @lines;

	## --- レス記事の場合
	} else {
		# ツリーソート「あり」
	 	if ($cf{top_sort}) {
			my $lx2;
			my $flg;
			foreach (@lines) {
				chomp;
				my ($no,$reno,$lx,$t,$e,$u,$n,$d,$m,$tm,$h,$a,$w,$oya,$smail,$res,$ex,$wi,$hi) = split(/<>/);

				if ($cf{bot_res} && $flg == 1 && $lx2 > $lx && $oya == $in{oya}) {
					$flg = 2;
					push(@new,"$count<>$in{no}<>$lx2<>$in{sub}<>$in{email}<>$in{url}<>$in{name}<>$date<>$in{message}<>$times<>$host<>$ango<>$in{wrap}<>$in{oya}<>$in{smail}<>0<>$ext<>$w<>$h<>\n");
				}

				# 同一ツリーの記事を @new に配列分割（直レス）
				if ($no == $in{no}) {
					$res++;
					push(@new,"$no<>$reno<>$lx<>$t<>$e<>$u<>$n<>$d<>$m<>$tm<>$h<>$a<>$w<>$oya<>$smail<>$res<>$ex<>$wi<>$hi<>\n");

				# 同一ツリーの記事を @new に配列分割
				} elsif ($in{oya} == $oya) {
					push(@new,"$_\n");

				# 別ツリーの記事を @tmp に配列分割
				} else {
					push(@tmp,"$_\n");
				}

				if ($no == $in{no}) {
					$flg = 1;
					$lx2 = $lx + 1;
					if (!$cf{bot_res}) {
						push(@new,"$count<>$in{no}<>$lx2<>$in{sub}<>$in{email}<>$in{url}<>$in{name}<>$date<>$in{message}<>$times<>$host<>$ango<>$in{wrap}<>$in{oya}<>$in{smail}<>0<>$ext<>$w<>$h<>\n");
					}
				}
			}
			if ($flg == 0) {
				close(DAT);
				&error("親投稿が見当たりません");
			}
			if ($cf{bot_res} && $flg != 2) {
				push(@new,"$count<>$in{no}<>$lx2<>$in{sub}<>$in{email}<>$in{url}<>$in{name}<>$date<>$in{message}<>$times<>$host<>$ango<>$in{wrap}<>$in{oya}<>$in{smail}<>0<>$ext<>$w<>$h<>\n");
			}
			# 配列最終結合
			push(@new,@tmp);

		# ツリーソート「なし」
		} else {
			my $lx2;
			my $flg = 0;
			foreach (@lines) {
				chomp;
				my ($no,$reno,$lx,$t,$e,$u,$n,$d,$m,$tm,$h,$a,$w,$oya,$smail,$res,$ex,$wi,$hi) = split(/<>/);

				if ($cf{bot_res} && $flg == 1 && $lx2 > $lx && ($reno ne $in{no} || $oya != $in{oya})) {
					$flg = 2;
					push(@new,"$count<>$in{no}<>$lx2<>$in{sub}<>$in{email}<>$in{url}<>$in{name}<>$date<>$in{message}<>$times<>$host<>$ango<>$in{wrap}<>$in{oya}<>$in{smail}<>0<>$ext<>$w<>$h<>\n");
				}

				# 直親記事
				if ($no == $in{no}) {
					$res++;
					push(@new,"$no<>$reno<>$lx<>$t<>$e<>$u<>$n<>$d<>$m<>$tm<>$h<>$a<>$w<>$oya<>$smail<>$res<>$ex<>$wi<>$hi<>\n");

				} else {
					push(@new,"$_\n");
				}

				if ($no == $in{no}) {
					$flg = 1;
       				$lx2 = $lx + 1;
					if (!$cf{bot_res}) {
       					push (@new,"$count<>$no<>$lx2<>$in{sub}<>$in{email}<>$in{url}<>$in{name}<>$date<>$in{message}<>$times<>$host<>$ango<>$in{wrap}<>$in{oya}<>$in{smail}<>0<>$ext<>$w<>$h<>\n");
					}
				}
			}
			if ($flg == 0) {
				close(DAT);
				error("親投稿が見当たりません");
			}
			if ($cf{bot_res} && $flg != 2) {
				push(@new,"$count<>$in{no}<>$lx2<>$in{sub}<>$in{email}<>$in{url}<>$in{name}<>$date<>$in{message}<>$times<>$host<>$ango<>$in{wrap}<>$in{oya}<>$in{smail}<>0<>$ext<>$w<>$h<>\n");
			}
    	}
	}

	# 最大記事数処理
	my @past;
	if (@new > $cf{maxlog}) {
		foreach (0 .. $#new) {
			# 最終尾ファイルを配列から抜き出し過去ログ配列へ
			my $log = pop(@new);
			unshift(@past,$log) if ($cf{pastkey});

			# 上位記事及び拡張子情報
			my ($no,$reno,$ex) = (split(/<>/,$log))[0,1,16];

			# 画像削除
			if ($ex) {
				unlink("$cf{upldir}/$no$ex");
				unlink("$cf{upldir}/$no-s$ex") if (-f "$cf{upldir}/$no-s$ex");
			}

			# ツリー単位か
			if (@new <= $cf{maxlog} && $reno eq 'no') {
				last;
			}
		}
	}

	# ログ更新
	unshift(@new,"$count<>$addr<>$times<>\n");
	seek(DAT, 0, 0);
	print DAT @new;
	truncate(DAT, tell(DAT));
	close(DAT);

	# 過去ログ処理
	make_pastlog(@past) if (@past > 0);

	# メール通知
	sendmail($count,$host) if ($cf{mailing});

	# 投稿完了
	after_msg($count,$date,$ext,$w,$h);
}

#-----------------------------------------------------------
#  投稿後メッセージ
#-----------------------------------------------------------
sub after_msg {
	my ($num,$date,$ext,$w,$h) = @_;

	# 引用色
	if ($cf{ref_col}) {
		$in{message} =~ s/([\>]|^)(&gt;[^<]*)/$1<font color="$cf{ref_col}">$2<\/font>/g;
	}

	# 画像付加
	$in{message} = put_image($num,$in{message},$ext,$w,$h) if ($ext);

	if ($in{email} && $in{smail} == 1) { $in{email} = '非表示'; }
	if($ua =~ /iPhone|Android|iPad/){
	open(IN,"$cf{tmpldir}/finish.html") or error("open err: finish.html");
	}else{
		open(IN,"$cf{tmpldir}/finishdk.html") or error("open err: finishdk.html");
	}
	
	open(FH,">> ./er.dat") or error("open err: er.dat");
	print FH "受理：$in{name} $in{sub} $in{message} \x0A\r\n";
	close(FH);


	my $tmpl = join('', <IN>);
	close(IN);

	$tmpl =~ s/!bbs_cgi!/$cf{bbs_cgi}/g;
	$tmpl =~ s/!sub!/$in{sub}/g;
	$tmpl =~ s/!num!/$num/g;
	$tmpl =~ s/!date!/$date/g;
	$tmpl =~ s/!name!/$in{name}/g;
	$tmpl =~ s/!email!/$in{email}/g;
	$tmpl =~ s/!url!/$in{url}/g;
	$tmpl =~ s/!msg!/$in{message}/g;

	print "Expires: -1\n";
	print "Cache-Control:public\n";
	print "Pragma:\n";
	print "Content-type: text/html; charset=utf-8\n\n";
	print $tmpl;
	exit;
}

#-----------------------------------------------------------
#  ユーザ記事修正
#-----------------------------------------------------------
sub user_edit {
	# POSTメソッド以外は不受理
	if ($ENV{REQUEST_METHOD} ne 'POST') { error("不正なリクエストです"); }

	# フォーム内容チェック
	$in{num} =~ s/\D//g;
	if ($in{num} eq '' || $in{pwd} eq '') {
		error("投稿NOまたはパスワードの記入モレがあります");
	}
	# 入力チェック
	if ($in{job} eq "edit") { check_input(); }

	my ($flg,$data,$ext,$w,$h,@new);
	open(DAT,"+< $cf{logfile}") or error("open err: $cf{logfile}");
	eval "flock(DAT, 2);";
	my $top = <DAT>;
	while (<DAT>) {
		chomp;
		my ($no,$re,$lx,$sub,$eml,$url,$nam,$dat,$msg,$lt,$ho,$pw,$wrp,$oya,$sml,$res,$ex,$wi,$hi) = split(/<>/);

		if ($in{num} == $no) {

			# 暗証キーなし
			if ($pw eq '') { last; }

			# 暗証キー不一致
			elsif (decrypt($in{pwd},$pw) != 1) { last; }

			$data = $_;
			$flg = 1;

			if ($in{job} ne "edit") {
				last;

			} else {

				# 画像削除
				if ($in{imgdel} && $ex) {
					unlink("$cf{upldir}/$no$ex");
					unlink("$cf{upldir}/$no-s$ex") if (-f "$cf{upldir}/$no-s$ex");
					$ex = $wi = $hi = '';
				}

				# 画像アップ
				($ext,$w,$h) = upload($in{num}) if ($in{upfile});
				if ($ext) {
					# 拡張子変更のとき
					if ($ex ne $ext) {
						unlink("$cf{upldir}/$no$ex");
						unlink("$cf{upldir}/$no-s$ex") if (-f "$cf{upldir}/$no-s$ex");
					}
					# 新画像情報
					($ex,$wi,$hi) = ($ext,$w,$h);
				}

				# 編集データ
				$_ = "$no<>$re<>$lx<>$in{sub}<>$in{email}<>$in{url}<>$in{name}<>$dat<>$in{message}<>$lt<>$ho<>$pw<>$in{wrap}<>$oya<>$in{smail}<>$res<>$ex<>$wi<>$hi<>";
			}
		}
		if ($in{job} eq "edit") { push(@new,"$_\n"); }
	}

	# 暗証キー判定
	if ($flg != 1) {
		close(DAT);
		error('削除修正キーが未設定か又は間違っています');
	}

	# 修正実行時
	if ($in{job} eq "edit") {
		unshift(@new,$top);
		seek(DAT, 0, 0);
		print DAT @new;
		truncate(DAT, tell(DAT));
		close(DAT);

		message("投稿の修正を完了しました。");

	# 修正フォーム
	} else {
		edit_form($data);
	}
}

#-----------------------------------------------------------
#  ユーザ記事削除
#-----------------------------------------------------------
sub user_dele {
	# POSTメソッド以外は不受理
	if ($ENV{REQUEST_METHOD} ne 'POST') { error("不正なリクエストです"); }

	# フォーム内容のチェック
	$in{num} =~ s/\D//g;
	if ($in{num} eq '' || $in{pwd} eq '') {
		error("投稿NOまたはパスワードの記入モレがあります");
	}

	# ログを読み込む
	my ($flg,$match,@new,@del);
	open(DAT,"+< $cf{logfile}") or error("open err: $cf{logfile}");
	eval "flock(DAT, 2);";
	my $top = <DAT>;
	while (<DAT>) {
		chomp;
		my ($no,$re,$lx,$sub,$eml,$url,$nam,$dat,$msg,$tim,$ho,$pw,$wr,$oya,$sml,$res,$ex,$wi,$hi) = split(/<>/);

		if ($in{num} == $no) {

			# パス未設定であれば終了
			if ($pw eq "") { last; }

			# 照合
			if (decrypt($in{pwd},$pw) == 1) {
				$flg = 1;
				if ($ex) {
					unlink("$cf{upldir}/$no$ex");
					unlink("$cf{upldir}/$no-s$ex") if (-f "$cf{upldir}/$no-s$ex");
				}
			} else {
				last;
			}

			if ($res == 0) { next; }
			$_ = "$no<>$re<>$lx<><s>投稿者削除</s><><><>(削除)<>$dat<>(投稿者により削除されました)<>$tim<>$ho<>DEL<>$wr<>$oya<>$sml<>$res<>$ex<>$wi<>$hi<>";

		} elsif ($no == $in{keyno}) {
			if ($res > 0) { $res--; }
			$_ = "$no<>$re<>$lx<>$sub<>$eml<>$url<>$nam<>$dat<>$msg<>$tim<>$ho<>$pw<>$wr<>$oya<>$sml<>$res<>$ex<>$wi<>$hi<>";
		}
		push(@new,"$_\n");
	}

	# エラー
	if (!$flg) {
		close(DAT);
		error("削除修正キーが未設定か又は認証できません");
	}

	# ログを更新
	unshift(@new,$top);
	seek(DAT, 0, 0);
	print DAT @new;
	truncate(DAT, tell(DAT));
	close(DAT);

	# 完了画面
	message("投稿を削除しました");
}

#-----------------------------------------------------------
#  メール送信
#-----------------------------------------------------------
sub sendmail {
	my ($count,$host) = @_;

	# 件名をMIMEエンコード
	my $msub = Jcode->new("[$count] $in{sub}",'utf-8')->mime_encode;

	# メール本文のタグ/改行を復元
	my $msg = $in{message};
	$msg =~ s/<br>/\n/g;
	$msg =~ s/&lt;/</g;
	$msg =~ s/&gt;/>/g;
	$msg =~ s/&quot;/"/g;
	$msg =~ s/&amp;/&/g;

	my $times = time;
	my $date = &get_time();

	# メール本文
	my $mbody = <<MAIL_BODY;
▼掲示板に投稿がありました。

投稿時間：$date
ホスト名：$host

投稿者名：$in{name}
Ｅメール：$in{email}
タイトル：$in{sub}
参照先  ：$in{url}

$msg
MAIL_BODY

	# 本文をJISコード変換
	$mbody = Jcode->new($mbody,'utf8')->jis;

	# FROM行の定義
	my $from = $in{email} || $cf{mailto};

	# sendmailコマンド
	my $scmd = "$cf{sendmail} -t -i";
	if ($cf{sendm_f} == 1) {
		$scmd .= " -f $from";
	}

	# sendmail起動
	open(MAIL,"| $scmd") or error("メール送信失敗");
	print MAIL "To: $cf{mailto}\n";
	print MAIL "From: $from\n";
	print MAIL "Subject: $msub\n";
	print MAIL "MIME-Version: 1.0\n";
	print MAIL "Content-type: text/plain; charset=ISO-2022-JP\n";
	print MAIL "Content-Transfer-Encoding: 7bit\n";
	print MAIL "X-Mailer: $cf{version}\n\n";
	print MAIL "$mbody\n";
	close(MAIL);
}

#-----------------------------------------------------------
#  過去ログ生成
#-----------------------------------------------------------
sub make_pastlog {
	my @past = @_;

	# 過去ログNOファイル
	open(NO,"+< $cf{nofile}") or error("open err: $cf{nofile}");
	eval "flock(NO, 2);";
	my $num = <NO>;

	# 過去ログを定義
	my $pastfile = "$cf{pastdir}/" . sprintf("%04d",$num) . ".cgi";

	# 過去ログを開く
	open(DAT,"+< $pastfile") or error("open err: $pastfile");
	eval "flock(DAT, 2);";
	my @data = <DAT>;

	# 規定行数オーバー時、次ファイル生成
	if (@data > $cf{max_line}) {

		# 過去ログを閉じる
		@data = ();
		close(DAT);

		# 過去NO更新
		seek(NO, 0, 0);
		print NO ++$num;
		truncate(NO, tell(NO));
		close(NO);

		$pastfile = "$cf{pastdir}/" . sprintf("%04d",$num) . ".cgi";

		open(DAT,"+> $pastfile");
		eval "flock(DAT, 2);";
		print DAT @past;
		close(DAT);

		chmod(0666, $pastfile);

	# 規定内
	} else {

		# 過去NOファイルを閉じる
		close(NO);

		# 過去ログ更新
		seek(DAT, 0, 0);
		print DAT @past;
		print DAT @data;
		truncate(DAT, tell(DAT));
		close(DAT);
	}
}

#-----------------------------------------------------------
#  入力チェック
#-----------------------------------------------------------
sub check_input {
	# 改行カット
	$in{sub}  =~ s/<br>//g;
	$in{name} =~ s/<br>//g;
	$in{pwd}  =~ s/<br>//g;
	$in{captcha} =~ s/<br>//g;
	$in{message} =~ s/(<br>)+$//g;

	# 画像
	if (!$cf{image_upl}) { $in{upfile} = ''; }

	# コード変換
	if ($cf{conv_code} == 1) {
		$in{name} = Jcode->new($in{name})->utf8;
		$in{sub}  = Jcode->new($in{sub})->utf8;
		$in{message} = Jcode->new($in{message})->utf8;
	}

	# スパムチェック
	if ($cf{no_wd}) { &no_wd; }
	if ($cf{jp_wd}) { &jp_wd; }
	if ($cf{urlnum} > 0) { &urlnum; }

	# 入力チェック
	my $err;
	if ($in{name} eq "") {
		$err .= "名前の入力モレです<br>";
	} elsif (length($in{name}) > 30) {
		$err .= "名前は全角で15文字までです<br>";
	}
	if ($in{message} eq "") {
		$err .= "コメントの入力モレです<br>";
	}
	if ($in{email} ne '' && $in{email} !~ /^[\w\.\-]+\@[\w\.\-]+\.[a-zA-Z]{2,6}$/) {
		$err .= "E-Mailの入力が不正です<br>";
	}
	if ($in{sub} eq "") {
		$err .= "タイトルの入力モレです<br>";
	} elsif (length($in{sub}) > $cf{sub_length}) {
		$err .= "タイトルは半角で$cf{sub_length}文字までです<br>";
	}
	if ($in{url} eq "http://") {
		$in{url} = "";
	} elsif ($in{url} ne '' && $in{url} !~ /^https?:\/\/[\w-.!~*'();\/?:\@&=+\$,%#]+$/) {
		$err .= "URL情報が不正です<br>";
	}
	if ($err) { error($err); }
}

#-----------------------------------------------------------
#  禁止ワードチェック
#-----------------------------------------------------------
sub no_wd {
	my $flg;
	foreach ( split(/,/, $cf{no_wd}) ) {
		if (index("$in{name} $in{sub} $in{message}",$_) >= 0) {
			$flg++;
			last;
		}
	}
	if ($flg) { error("禁止ワードが含まれています"); }
}

#-----------------------------------------------------------
#  日本語チェックsjisからutf-8に改造半角カナひらがなカタカナ漢字
#-----------------------------------------------------------
sub jp_wd {
	my $nihongo = decode('UTF-8', $in{message});
#	if ($in{message} !~ /[\x81-\x9F\xE0-\xFC][\x40-\x7E\x80-\xFC]/) {
	if ($nihongo !~ /[\x{ff66}-\x{ff9d}]|[\x{3041}-\x{3096}]|[\x{30A1}-\x{30FA}]|[\x{3005}\x{3400}-\x{9FFF}\x{F900}-\x{FAFF}]|[\x{D840}-\x{D87F}][\x{DC00}-\x{DFFF}]/) {

			open(FH,">> ./er.dat") or error("open err: er.dat");
			print FH "阻止：$in{name} $in{sub} $in{message} \x0A\r\n";
			close(FH);
		error("メッセージに日本語が含まれていません");



	}
}

#-----------------------------------------------------------
#  URL個数チェック
#-----------------------------------------------------------
sub urlnum {
	my $com = $in{message};
	my $num = ($com =~ s|(https?://)|$1|ig);
	if ($num > $cf{urlnum}) {
		error("コメント中のURLアドレスは最大$cf{urlnum}個までです");
	}
}

#-----------------------------------------------------------
#  修正フォーム
#-----------------------------------------------------------
sub edit_form {
	my $data = shift;
	my ($no,$re,$lx,$sub,$eml,$url,$nam,$dat,$msg,$lt,$ho,$pw,$wrp,$oya,$sml,$res,$ex,$w,$h) = split(/<>/, $data);
	$msg =~ s/<br>/\n/g;

	my %ops = (0 =>'表示', 1 =>'非表示');
	my $op_smail;
	foreach (0,1) {
		if ($sml == $_) {
			$op_smail .= qq|<option value="$_" selected>$ops{$_}\n|;
		} else {
			$op_smail .= qq|<option value="$_">$ops{$_}\n|;
		}
	}
	if($ua =~ /iPhone|Android|iPad/){
	open(IN,"$cf{tmpldir}/edit.html") or error("open err: edit.html");
	}else{
	     open(IN,"$cf{tmpldir}/editdk.html") or error("open err: editdk.html");
	}
	my $tmpl = join('', <IN>);
	close(IN);

	# 画像投稿用
	if (!$cf{image_upl}) {
		$tmpl =~ s/<!-- image_begin -->.+<!-- image_end -->//s;
	} elsif ($ex) {
		# 画像編集用
		my $image;
		$image .= qq|&nbsp; [<a href="$cf{uplurl}/$no$ex" target="_blank">画像</a>]\n|;
		$image .= qq|<input type="checkbox" name="imgdel" value="1">削除\n|;

		$tmpl =~ s/<!-- image -->/$image/g;
	}

	$tmpl =~ s/!([a-z]+_cgi)!/$cf{$1}/g;
	$tmpl =~ s/!name!/$nam/g;
	$tmpl =~ s/!email!/$eml/g;
	$tmpl =~ s/!sub!/$sub/g;
	$tmpl =~ s/!msg!/$msg/g;
	$tmpl =~ s/!url!/$url/g;
	$tmpl =~ s/!num!/$in{num}/g;
	$tmpl =~ s/!pwd!/$in{pwd}/g;
	$tmpl =~ s/<!-- op_smail -->/$op_smail/g;
	
	print "Expires: -1\n";
	print "Cache-Control:public\n";
	print "Pragma:\n";
	print "Content-type: text/html; charset=utf-8\n\n";
	print $tmpl;
	exit;
}

#-----------------------------------------------------------
#  完了メッセージ画面
#-----------------------------------------------------------
sub message {
	my $msg = shift;
	if($ua =~ /iPhone|Android|iPad/){
	open(IN,"$cf{tmpldir}/message.html") or error("open err: message.html");
	}else{
	     open(IN,"$cf{tmpldir}/messagedk.html") or error("open err: messagedk.html");
	}
	


	my $tmpl = join('', <IN>);
	close(IN);

	$tmpl =~ s/!message!/$msg/;
	$tmpl =~ s/!bbs_cgi!/$cf{bbs_cgi}/;
	
	print "Expires: -1\n";
	print "Cache-Control:public\n";
	print "Pragma:\n";
	print "Content-type: text/html; charset=utf-8\n\n";
	print $tmpl;
	exit;
}

#-----------------------------------------------------------
#  ホストチェック
#-----------------------------------------------------------
sub check_host {
	# IP/ホスト取得
	my $host = $ENV{REMOTE_HOST};
	my $addr = $ENV{REMOTE_ADDR};
	if ($cf{gethostbyaddr} && ($host eq "" || $host eq $addr)) {
		$host = gethostbyaddr(pack("C4", split(/\./, $addr)), 2);
	}

	# IPチェック
	my $flg;
	foreach ( split(/\s+/, $cf{deny_addr}) ) {
		s/\./\\\./g;
		s/\*/\.\*/g;

		if ($addr =~ /^$_/i) { $flg++; last; }
	}
	if ($flg) {
		error("アクセスを許可されていません");

	# ホストチェック
	} elsif ($host) {

		foreach ( split(/\s+/, $cf{deny_host}) ) {
			s/\./\\\./g;
			s/\*/\.\*/g;

			if ($host =~ /$_$/i) { $flg++; last; }
		}
		if ($flg) {
			error("アクセスを許可されていません");
		}
	}
	$host ||= $addr;

	return ($host,$addr);
}

#-----------------------------------------------------------
#  crypt暗号
#-----------------------------------------------------------
sub encrypt {
	my $in = shift;

	my @wd = ('a'..'z', 'A'..'Z', '0'..'9', '.', '/');
	srand;
	my $salt = $wd[int(rand(@wd))] . $wd[int(rand(@wd))];
	crypt($in, $salt) || crypt ($in, '$1$' . $salt);
}

#-----------------------------------------------------------
#  crypt照合
#-----------------------------------------------------------
sub decrypt {
	my ($in,$dec) = @_;

	my $salt = $dec =~ /^\$1\$(.*)\$/ ? $1 : substr($dec, 0, 2);
	if (crypt($in, $salt) eq $dec || crypt($in, '$1$' . $salt) eq $dec) {
		return 1;
	} else {
		return 0;
	}
}

#-----------------------------------------------------------
#  クッキー発行
#-----------------------------------------------------------
sub set_cookie {
	my @data = @_;

	my ($sec,$min,$hour,$mday,$mon,$year,$wday,undef,undef) = gmtime(time + 60*24*60*60);
	my @mon  = qw|Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec|;
	my @week = qw|Sun Mon Tue Wed Thu Fri Sat|;

	# 時刻フォーマット
	my $gmt = sprintf("%s, %02d-%s-%04d %02d:%02d:%02d GMT",
				$week[$wday],$mday,$mon[$mon],$year+1900,$hour,$min,$sec);

	# URLエンコード
	my $cook;
	foreach (@data) {
		s/(\W)/sprintf("%%%02X", unpack("C", $1))/eg;
		$cook .= "$_<>";
	}

	print "Set-Cookie: $cf{cookie_id}=$cook; expires=$gmt\n";
}

#-----------------------------------------------------------
#  添付アップロード
#-----------------------------------------------------------
sub upload {
	my $no = shift;

	# サムネイル機能
	require './lib/thumb.pl' if ($cf{thumbnail});

	# 拡張子取得
	my $ext = $cgi->param_filename('upfile') =~ /(\.[^\\\/:\.]+)$/ ? $1 : &error("拡張子不明");
	$ext =~ tr/A-Z/a-z/;
	if ($ext eq '.jpeg') { $ext = '.jpg'; }

	# mimeタイプ
	my $mime = $cgi->param_mime('upfile');

	# 整合性チェック
	unless (($mime =~ /^image\/gif$/i and $ext eq '.gif') or ($mime =~ /^image\/p?jpe?g$/i and $ext eq '.jpg') or ($mime =~ /^image\/(x-)?png$/i and $ext eq '.png')) {
		error('この画像は受理できません');
	}

	# 添付ファイル定義
	my $upfile = "$cf{upldir}/$no$ext";

	# アップロード書き込み
	my $buf;
	open(UP,"+> $upfile") or error("up err: $upfile");
	binmode(UP);
	print UP $in{upfile};
	close(UP);

	chmod(0666, $upfile);

	# 画像サイズ取得
	my ($flg,$w,$h);
	if ($ext eq ".jpg") { ($w,$h) = j_size($upfile); $flg++; }
	elsif ($ext eq ".gif") { ($w,$h) = g_size($upfile); $flg++; }
	elsif ($ext eq ".png") { ($w,$h) = p_size($upfile); $flg++; }

	# サムネイル化
	if ($flg && $cf{thumbnail}) {
		($w,$h) = resize($w,$h);
		my $thumb = "$cf{upldir}/$no-s$ext";
		make_thumb($upfile,$thumb,$w,$h);
	}

	# 結果を返す
	return ($ext,$w,$h);
}

#-----------------------------------------------------------
#  JPEGサイズ認識
#-----------------------------------------------------------
sub j_size {
	my $jpg = shift;

	my ($h, $w, $t);
	open(IMG,"$jpg");
	binmode(IMG);
	read(IMG, $t, 2);
	while (1) {
		read(IMG, $t, 4);
		my ($m, $c, $l) = unpack("a a n", $t);

		if ($m ne "\xFF") {
			$w = $h = 0;
			last;
		} elsif ((ord($c) >= 0xC0) && (ord($c) <= 0xC3)) {
			read(IMG, $t, 5);
			($h, $w) = unpack("xnn", $t);
			last;
		} else {
			read(IMG, $t, ($l - 2));
		}
	}
	close(IMG);

	return ($w,$h);
}

#-----------------------------------------------------------
#  GIFサイズ認識
#-----------------------------------------------------------
sub g_size {
	my $gif = shift;

	my $data;
	open(IMG,"$gif");
	binmode(IMG);
	sysread(IMG, $data, 10);
	close(IMG);

	if ($data =~ /^GIF/) { $data = substr($data, -4); }
	my $w = unpack("v", substr($data, 0, 2));
	my $h = unpack("v", substr($data, 2, 2));

	return ($w,$h);
}

#-----------------------------------------------------------
#  PNGサイズ認識
#-----------------------------------------------------------
sub p_size {
	my $png = shift;

	my $data;
	open(IMG,"$png");
	binmode(IMG);
	read(IMG, $data, 24);
	close(IMG);

	my $w = unpack("N", substr($data, 16, 20));
	my $h = unpack("N", substr($data, 20, 24));

	return ($w,$h);
}

