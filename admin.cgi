#!/usr/bin/perl

#┌─────────────────────────────────
#│ Web Forum : admin.cgi - 2013/01/27
#│ Copyright (c) KentWeb
#│ http://www.kent-web.com/
#└─────────────────────────────────

# モジュール宣言
use strict;
use CGI::Carp qw(fatalsToBrowser);
use lib "./lib";
use CGI::Minimal;

# 外部ファイル取込み
require './init.cgi';
my %cf = &init;

# データ受理
CGI::Minimal::max_read_size($cf{maxdata});
my $cgi = CGI::Minimal->new;
&err('容量オーバー') if ($cgi->truncated);
my %in = &parse_form($cgi);

# 認証
&check_passwd;

# 処理分岐
if ($in{mode} eq "edit" && $in{no}) { &edit_data; }
if ($in{mode} eq "dele" && $in{no}) { &dele_data; }
if ($in{list_data}) { &list_data; }

# メニュー画面
&menu_html;

#-----------------------------------------------------------
#  メニュー画面
#-----------------------------------------------------------
sub menu_html {
	&header("メニューTOP");

	print <<EOM;
<div align="center">
<p>選択ボタンを押してください。</p>
<form action="$cf{admin_cgi}" method="post">
<input type="hidden" name="pass" value="$in{pass}">
<table class="form">
<tr>
	<th></th>
	<th width="250">処理メニュー</th>
</tr><tr>
	<td><input type="submit" name="list_data" value="選択"></td>
	<td>記事リスト（修正/削除）</td>
</tr><tr>
	<td><input type="button" value="選択" onclick="javascript:window.location='$cf{bbs_cgi}'"></td>
	<td>掲示板へ戻る</td>
</tr><tr>
	<td><input type="button" value="選択" onclick="javascript:window.location='$cf{admin_cgi}'"></td>
	<td>ログアウト</td>
</tr>
</table>
</form>
</div>
</body>
</html>
EOM
	exit;
}

#-----------------------------------------------------------
#  記事リスト
#-----------------------------------------------------------
sub list_data {
	# ページ数
	my $page = $in{page} || '0';
	foreach ( $cgi->param() ) {
		if (/^page:(\d+)$/) {
			$page = $1;
			last;
		}
	}

	&header("メニューTOP ＞ 記事リスト");
	&back_btn;
	print <<EOM;
<div class="ttl">■ 記事リスト</div>
<ul>
<li>処理を選択して送信ボタンを押してください。
<li>ツリーの先頭記事を削除すると、ツリーごと一括削除されます。
</ul>
<form action="$cf{admin_cgi}" method="post">
<input type="hidden" name="pass" value="$in{pass}">
<input type="hidden" name="list_data" value="1">
処理 <select name="mode">
<option value="edit">修正
<option value="dele">削除
</select>
<input type="submit" value="送信する">
<dl>
EOM

	my $i = 0;
	my $x = 0;
	open(IN,"$cf{logfile}") or &err("open err: $cf{logfile}");
	my $top = <IN>;
	while (<IN>) {
		my ($no,$re,$lx,$sub,$eml,$url,$nam,$date,$msg,$t,$hos,$pw,$wr,$oya,$sml,$res,$ex,$w,$h) = split(/<>/);
		if ($re == 0) { $i++; }
		next if ($i < $page + 1);
		next if ($i > $page + $cf{p_tree});

		my $sp;
		if ($lx) { $sp = "&nbsp;&nbsp;&nbsp;" x $lx; }
		$msg =~ s/<br>/ /g;
		if (length($msg) > 60) { $msg = substr($msg,0,58); $msg .= '..'; }

		print "<dt>";
		print "<hr>" if ($no == $oya);
		print qq|$sp<input type="checkbox" name="no" value="$no">[$no] <b>$sub</b> - <B>$nam</B> $date\n|;
		print qq|&nbsp;<span class="host">[$hos]</span></dt>\n|;
		print qq|<dd class="msg">$msg</dd>\n|;

	}
	close(IN);

	print "<dt><hr></dl>\n";

	my $next = $page + $cf{p_tree};
	my $back = $page - $cf{p_tree};

	if ($back >= 0) {
		print qq|<input type="submit" name="page:$back" value="前画面">\n|;
	}
	if ($next < $i) {
		print qq|<input type="submit" name="page:$next" value="次画面">\n|;
	}

	print <<EOM;
</form>
</body>
</html>
EOM
	exit;
}

#-----------------------------------------------------------
#  編集画面
#-----------------------------------------------------------
sub edit_data {
	# 修正実行
	if ($in{submit}) { &edit_log; }

	# ログを開く
	my $log;
	open(IN,"$cf{logfile}") or &err("open err: $cf{logfile}");
	my $top = <IN>;
	while (<IN>) {
		chomp;
		my ($no,$re,$x,$sub,$eml,$url,$nam,$dat,$msg,$t,$ho,$pw,$wr,$oya,$sml,$res,$ex,$w,$h) = split(/<>/);

		if ($in{no} == $no) {
			$log = $_;
			last;
		}
	}
	close(IN);

	# 記事分解
	my ($no,$re,$x,$sub,$eml,$url,$nam,$dat,$msg,$t,$ho,$pw,$wr,$oya,$sml,$res,$ex,$w,$h) = split(/<>/, $log);
	$msg =~ s/<br>/\n/g;

	# 編集フォームを出力
	&header("メニューTOP ＞ 記事リスト ＞ 編集画面");
	&back_btn('list_data');
	print <<EOM;
<div class="ttl">■ 記事リスト ＞ 編集画面</div>
<p>▼変更したい部分のみ修正し送信ボタンを押してください。</p>
<form action="$cf{admin_cgi}" method="post">
<input type="hidden" name="mode" value="edit">
<input type="hidden" name="no" value="$in{no}">
<input type="hidden" name="pass" value="$in{pass}">
<table class="form">
<tr>
	<th>投稿者</th>
	<td><input type="text" name="name" value="$nam" size="28"></td>
</tr><tr>
	<th>Ｅメール</th>
	<td><input type="text" name="email" value="$eml" size="28">
		<select name="smail">
EOM

	my @sm = ('表示', '非表示');
	foreach (0,1) {
		if ($sml == $_) {
			print "<option value=\"$_\" selected>$sm[$_]\n";
		} else {
			print "<option value=\"$_\">$sm[$_]\n";
		}
	}

	print <<EOM;
	</select></td>
</tr><tr>
	<th>タイトル</th>
	<td><input type="text" name="sub" value="$sub" size="40"></td>
</tr><tr>
	<th>メッセージ</th>
	<td><textarea name="message" cols="64" rows="8">$msg</textarea></td>
</tr><tr>
	<th>参照先</th>
	<td><input type="text" name="url" value="$url" size="55"></td>
EOM

	if ($ex) {
		print qq|</tr><tr><th>UP画像</th>\n|;
		print qq|<td>[<a href="$cf{upldir}/$no$ex" target="_blank">閲覧</a>]\n|;
		print qq|<input type="checkbox" name="imgdel" value="1">削除</td>\n|;
	}

	print <<EOM;
</tr>
</table>
<input type="submit" name="submit" value=" 送信する ">
</form>
</body>
</html>
EOM
	exit;
}

#-----------------------------------------------------------
#  編集実行
#-----------------------------------------------------------
sub edit_log {
	# ログを開く
	my @data;
	open(DAT,"+< $cf{logfile}") or &err("open err: $cf{logfile}");
	eval "flock(DAT, 2);";
	my $top = <DAT>;
	while (<DAT>) {
		chomp;
		my ($no,$re,$x,$sub,$eml,$url,$nam,$dat,$msg,$t,$ho,$pw,$wr,$oya,$sml,$res,$ex,$w,$h) = split(/<>/);

		if ($in{no} == $no) {

			# 画像削除
			if ($in{imgdel} && $ex) {
				unlink("$cf{upldir}/$no$ex") if ($ex);
				unlink("$cf{upldir}/$no-s$ex") if ($ex && -f "$cf{upldir}/$no-s$ex");
				$ex = $w = $h = '';
			}

			$_ = "$no<>$re<>$x<>$in{sub}<>$in{email}<>$in{url}<>$in{name}<>$dat<>$in{message}<>$t<>$ho<>$pw<><>$oya<>$in{smail}<>$res<>$ex<>$w<>$h<>";
		}
		push(@data,"$_\n");
	}

	# ログを更新
	unshift(@data,$top);
	seek(DAT, 0, 0);
	print DAT @data;
	truncate(DAT, tell(DAT));
	close(DAT);

	# 完了
	&message("修正を完了しました");
}

#-----------------------------------------------------------
#  削除処理
#-----------------------------------------------------------
sub dele_data {
	my %del;
	foreach ( $cgi->param('no') ) {
		$del{$_}++;
	}

	# ログを開く
	my @data;
	open(DAT,"+< $cf{logfile}") or &err("open err: $cf{logfile}");
	eval "flock(DAT, 2);";
	my $top = <DAT>;
	while (<DAT>) {
		my ($no,$re,$x,$sub,$eml,$url,$nam,$dat,$msg,$t,$ho,$pw,$wr,$oya,$sml,$res,$ex,$w,$h) = split(/<>/);

		if (defined($del{$no}) or defined($del{$oya})) {
			unlink("$cf{upldir}/$no$ex") if ($ex);
			unlink("$cf{upldir}/$no-s$ex") if ($ex && -f "$cf{upldir}/$no-s$ex");
			next;
		}

		push(@data,$_);
	}

	# ログを更新
	unshift(@data,$top);
	seek(DAT, 0, 0);
	print DAT @data;
	truncate(DAT, tell(DAT));
	close(DAT);

	# 初期画面に戻る
	&list_data;
}

#-----------------------------------------------------------
#  パスワード認証
#-----------------------------------------------------------
sub check_passwd {
	# パスワードが未入力の場合は入力フォーム画面
	if ($in{pass} eq "") {
		&enter_form;

	# パスワード認証
	} elsif ($in{pass} ne $cf{password}) {
		&err("認証できません");
	}
}

#-----------------------------------------------------------
#  入室画面
#-----------------------------------------------------------
sub enter_form {
	&header("入室画面");
	print <<EOM;
<div align="center">
<form action="$cf{admin_cgi}" method="post">
<table width="380" style="margin-top:50px">
<tr>
	<td height="40" align="center">
		<fieldset><legend>管理パスワード入力</legend>
		<br>
		<input type="password" name="pass" value="" size="20">
		<input type="submit" value=" 認証 ">
		<br><br>
		</fieldset>
	</td>
</tr>
</table>
</form>
<script language="javascript">
<!--
self.document.forms[0].pass.focus();
//-->
</script>
</div>
</body>
</html>
EOM
	exit;
}

#-----------------------------------------------------------
#  HTMLヘッダー
#-----------------------------------------------------------
sub header {
	my $ttl = shift;

	print <<EOM;
Content-type: text/html; charset=utf-8

<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<meta http-equiv="content-style-type" content="text/css">
<style type="text/css">
<!--
body,td,th { font-size:80%; background:#f0f0f0; }
div.ttl { border-bottom:1px #003264 solid; padding:3px 0; font-weight:bold; color:#003264; }
table.form { margin:1em 0; border-collapse:collapse; }
table.form th { border:1px #393939 solid; padding:5px; background:#9999cc; }
table.form td { border:1px #393939 solid; padding:5px; background:#fff; }
span.host { color:green; }
dd.msg { color:navy; font-size:11px; }
-->
</style>
<title>$ttl</title>
</head>
<body>
EOM
}

#-----------------------------------------------------------
#  戻りボタン
#-----------------------------------------------------------
sub back_btn {
	my ($mode) = @_;

	print <<EOM;
<div align="right">
<form action="$cf{admin_cgi}" method="post">
<input type="hidden" name="pass" value="$in{pass}">
@{[ $mode ? qq|<input type="submit" name="$mode" value="&lt; 前画面">| : "" ]}
<input type="submit" value="&lt; メニュー">
</form>
</div>
EOM
}

#-----------------------------------------------------------
#  エラー
#-----------------------------------------------------------
sub err {
	my $msg = shift;

	&header("ERROR!");
	print <<EOM;
<div align="center">
<hr width="350">
<h3>ERROR!</h3>
<p style="color:#dd0000">$msg</p>
<hr width="350">
<form>
<input type="button" value="前画面に戻る" onclick="history.back()">
</form>
</div>
</body>
</html>
EOM
	exit;
}

#-----------------------------------------------------------
#  メッセージ表示
#-----------------------------------------------------------
sub message {
	my ($msg,$btn) = @_;

	&header("処理完了");
	print <<EOM;
<b class="ttl">■処理完了</b>
<hr color="#00f078">
<p>$msg</p>
<form action="$cf{admin_cgi}" method="post">
<input type="hidden" name="pass" value="$in{pass}">
<input type="hidden" name="list_data" value="1">
<input type="submit" value="メニューに戻る">
</form>
</body>
</html>
EOM
	exit;
}

