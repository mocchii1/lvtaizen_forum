# モジュール宣言/変数初期化
use strict;
my %cf;
#┌─────────────────────────────────

#│ Web Forum : init.cgi - 2013/05/31
#│ Copyright (c) KentWeb
#│ http://www.kent-web.com/
#└─────────────────────────────────
$cf{version} = 'Web Forum v7.11';
#┌─────────────────────────────────
#│[ 注意事項 ]
#│ 1. このスクリプトはフリーソフトです。このスクリプトを使用した
#│    いかなる損害に対して作者は一切の責任を負いません。
#│ 2. 設置に関する質問はサポート掲示板にお願いいたします。
#│    直接メールによる質問は一切お受けいたしておりません。
#└─────────────────────────────────

#============================================================
# ■ 基本設定
#============================================================

# パスワード (半角英数字で)
$cf{password} = 'Lvkanri777';

# パスワード制限をする場合入室パスワード設定
# → 空欄の場合はパスワード制限なし
$cf{enter_pwd} = '';

# パスワード制限時のセッションの許容時間（分単位）
# → 入室後からアクセス可能時間
$cf{sestime} = 60;

# 画像アップを許可する
# 0=no 1=yes
$cf{image_upl} = 1;

# サムネイル画像を作成する（要：Image::Magick）
# → 縮小画像を自動生成し、画像記事の表示速度を軽くする機能
# 0=no 1=yes
$cf{thumbnail} = 0;

# 掲示板タイトル
$cf{bbs_title} = 'ラスベガス大全フォーラム';

# 最大記事数
$cf{maxlog} = 20000;

# スクリプトURL【URLパス】
$cf{bbs_cgi} = './wforum.cgi';

# 管理ファイルURL【URLパス】
$cf{admin_cgi} = './admin.cgi';

# 書込ファイルURL【URLパス】
$cf{regist_cgi} = './regist.cgi';

# ログファイル【サーバパス】
$cf{logfile} = './data/log.cgi';

# セッションファイル【サーバパス】
$cf{sesfile} = './data/ses.cgi';

# 画像ディレクトリ（画像アップを許可するとき）
# → 順に、サーバパス、URLパス
$cf{upldir} = './upl';
$cf{uplurl} = './upl';

# テンプレートディレクトリ【サーバパス】
$cf{tmpldir} = "./tmpl";

# URL自動リンク (0=no 1=yes)
$cf{autolink} = 1;

# 記事にNEWマークを付ける時間（時間単位）
$cf{new_time} = 48;

# NEWマークの表示形態
#  → 画像を使用する場合には $newmark = '<img src="./img/new.gif">';
#     というように IMGタグを記述してもよい
$cf{new_mark} = '<font color="#ff3300">new!</font>';

# 家アイコン【URLパス】
$cf{ico_home} = "./icon/home.gif";

# 記事番号の色（標準）
$cf{num_st_col} = "#008000";

# 記事番号の色（強調時）
$cf{num_em_col} = "#dd0000";

# 新着表示のページ当たり表示件数
$cf{new_pgbtn} = 20;

# 頁あたりツリー表示数
$cf{p_tree} = 20;

# リストに表示する「記事タイトル」の最大長（文字数：半角文字換算）
$cf{sub_length} = 200;

# レスがついたらツリー毎トップへ移動 (0=no 1=yes)
$cf{top_sort} = 1;

# レスは下から順に付ける (0=no 1=yes)
$cf{bot_res} = 1;

# 引用部色変更
#  → ここに色指定を行うと「引用部」を色変更します
#  → この機能を使用しない場合は何も記述しないで下さい ($refcol="";)
$cf{ref_col} = "#804000";

# 戻り先URL【URLパス】
$cf{homepage} = "../../index.html";

# 文字コード自動判別（0=no 1=yes）
# → フォーム入力の文字コード判別を行う場合
$cf{conv_code} = 0;

# 画像ファイルの最大表示の大きさ（単位：ピクセル）
# → これを超える画像は縮小表示します
$cf{max_img_w} = 250;	# 横幅
$cf{max_img_h} = 150;	# 縦幅

# 投稿があるとメール通知する : sendmail必須
#  0 : 通知しない
#  1 : 通知する
$cf{mailing} = 0;

# メール通知する際のメールアドレス
$cf{mailto} = 'xxx@xxx.xx';

# sendmailパス（メール通知する時）
$cf{sendmail} = '/usr/lib/sendmail';

# sendmailの -fコマンドが必要な場合
# 0=no 1=yes
$cf{sendm_f} = 0;

# ツリーのヘッダー記号
$cf{treehead} = "▼";

# クッキーID（特に変更しなくてよい）
# → クッキー保存名
$cf{cookie_id}  = "web_forum";  # 投稿フォーム用
$cf{cookie_id3} = "wf_passwd";  # アクセス制限用

# ホスト取得方法
# 0 : gethostbyaddr関数を使わない
# 1 : gethostbyaddr関数を使う
$cf{gethostbyaddr} = 0;

# アクセス制限（半角スペースで区切る、アスタリスク可）
#  → 拒否ホスト名を記述（後方一致）【例】*.anonymizer.com
$cf{deny_host} = '';
#  → 拒否IPアドレスを記述（前方一致）【例】210.12.345.*
$cf{deny_addr} = '';

# 禁止ワード
# → 投稿時禁止するワードをコンマで区切る
$cf{no_wd} = '';

# 日本語チェック（投稿時日本語が含まれていなければ拒否する）
# 0=No  1=Yes
$cf{jp_wd} = 1;

# URL個数チェック
# → 投稿コメント中に含まれるURL個数の最大値
$cf{urlnum} = 2;

# １度の投稿で受理できる最大サイズ (bytes)
# → 1024Byte = 100KB
$cf{maxdata} = 512000;

# 投稿制限
#  0 : しない
#  1 : 同一IPアドレスからの投稿間隔を制限する
#  2 : 全ての投稿間隔を制限する
$cf{regCtl} = 0;

# 制限投稿間隔（秒数）
#  → $regCtl での投稿間隔
$cf{wait} = 60;

# -------------------------------------------------------------- #
# [ 以下は「過去ログ」機能を使用する場合の設定 ]

# 過去ログ機能 (0=no 1=yes)
$cf{pastkey} = 0;

# 過去ログカウントファイル【サーバパス】
$cf{nofile} = './data/pastno.dat';

# 過去ログのディレクトリ【サーバパス】
$cf{pastdir} = './data/past';

# 過去ログ１ページ当りの最大行数
#  → これを超えると自動的に次ファイルを生成します
$cf{max_line} = 650;

# -------------------------------------------------------------- #
# [ 以下は「画像認証機能」機能（スパム対策）を使用する場合の設定 ]
#
# 画像認証機能の使用
# 0 : しない
# 1 : ライブラリ版（pngren.pl）
# 2 : モジュール版（GD::SecurityImage + Image::Magick）→ Image::Magick必須
$cf{use_captcha} = 1;

# 認証用画像生成ファイル【URLパス】
$cf{captcha_cgi} = './captcha.cgi';

# 画像認証プログラム【サーバパス】
$cf{captcha_pl} = './lib/captcha.pl';
$cf{captsec_pl} = './lib/captsec.pl';
$cf{pngren_pl}  = './lib/pngren.pl';

# 画像認証機能用暗号化キー（暗号化/復号化をするためのキー）
# → 適当に変更してください。
$cf{captcha_key} = 'captchaforum';

# 投稿キー許容時間（分単位）
# → 投稿フォーム表示後、送信ボタンが押されるまでの可能時間。
$cf{cap_time} = 1440;

# 投稿キーの文字数
# ライブラリ版 : 4～8文字で設定
# モジュール版 : 6～8文字で設定
$cf{cap_len} = 6;

# 画像/フォント格納ディレクトリ【サーバパス】
$cf{bin_dir} = './lib/bin';

# [ライブラリ版] 画像ファイル [ ファイル名のみ ]
$cf{si_png} = "degi.png";

# [モジュール版] 画像フォント [ ファイル名のみ ]
$cf{font_ttl} = "tempest.ttf";

#============================================================
# ■ 設定完了
#============================================================

# 著作権表記（削除厳禁）
$cf{copyright} = <<EOM;
<p style="margin-top:2.5em;text-align:center;font-family:Verdana,Helvetica,Arial;font-size:10px;">
- <a href="http://www.kent-web.com/" target="_top">WebForum</a> -
</p>
EOM

# ハッシュを返す
sub init { return %cf; }

#-----------------------------------------------------------
#  フォームデコード
#-----------------------------------------------------------
sub parse_form {
	my $cgi = shift;

	my %in;
	foreach ( $cgi->param() ) {
		my $val = $cgi->param($_);

		if ($_ ne 'upfile') {
			# 無効化
			$val =~ s/&/&amp;/g;
			$val =~ s/</&lt;/g;
			$val =~ s/>/&gt;/g;
			$val =~ s/"/&quot;/g;
			$val =~ s/'/&#39;/g;

			# 改行処理
			$val =~ s/\r\n/<br>/g;
			$val =~ s/\r/<br>/g;
			$val =~ s/\n/<br>/g;
		}
		$in{$_} = $val;
	}
	return %in;
}

#-----------------------------------------------------------
#  エラー処理
#-----------------------------------------------------------
sub error {
	my $msg = shift;

	open(IN,"$cf{tmpldir}/error.html") or die;
	my $tmpl = join('', <IN>);
	close(IN);

	$tmpl =~ s/!message!/$msg/g;

	print "Content-type: text/html; charset=utf-8\n\n";
	print $tmpl;
	exit;
}

#-----------------------------------------------------------
#  時間取得
#-----------------------------------------------------------
sub get_time {
	my ($time,$log) = @_;
	$time ||= time;

	# 時間取得
	my ($sec,$min,$hour,$day,$mon,$year,$wday) = (localtime($time))[0..6];

	# 短縮型
	if ($log eq "log") {
		sprintf("%02d/%02d/%02d-%02d:%02d",
				$year-100,$mon+1,$day,$hour,$min);

	# 標準型
	} else {
		my @week = qw|Sun Mon Tue Wed Thu Fri Sat|;
		sprintf("%04d/%02d/%02d(%s) %02d:%02d:%02d",
				$year+1900,$mon+1,$day,$week[$wday],$hour,$min,$sec);
	}
}

#-----------------------------------------------------------
#  画像リサイズ
#-----------------------------------------------------------
sub resize {
	my ($w,$h) = @_;

	# 画像表示縮小
	if ($w > $cf{max_img_w} || $h > $cf{max_img_h}) {
		my $w2 = $cf{max_img_w} / $w;
		my $h2 = $cf{max_img_h} / $h;
		my $key;
		if ($w2 < $h2) { $key = $w2; } else { $key = $h2; }
		$w = int ($w * $key) || 1;
		$h = int ($h * $key) || 1;
	}

	return ($w,$h);
}

#-----------------------------------------------------------
#  パスワード制限
#-----------------------------------------------------------
sub passwd {
	my %in = @_;

	# 入室フォーム指定のとき
	if ($in{mode} eq 'enter') { pwd_form(); }

	# 時間取得
	my $now = time;

	# ログインのとき
	if ($in{login}) {
		# 認証
		if ($in{pw} ne $cf{enter_pwd}) { error("認証できません"); }

		# セッション発行
		my @wd = (0 .. 9, 'a' .. 'z', 'A' .. 'Z', '_');
		my $ses;
		srand;
		for (1 .. 25) {	$ses .= $wd[int(rand(@wd))]; }

		# セッション更新
		my @log;
		open(DAT,"+< $cf{sesfile}") or error("write err: $cf{sesfile}");
		eval 'flock(DAT, 2);';
		while(<DAT>) {
			chomp;
			my ($id,$time) = split(/\t/);
			next if ($now - $time > $cf{sestime} * 60);

			push(@log,"$_\n");
		}
		unshift(@log,"$ses\t$now\n");
		seek(DAT, 0, 0);
		print DAT @log;
		truncate(DAT, tell(DAT));
		close(DAT);

		# クッキー格納
		print "Set-Cookie: $cf{cookie_id3}=$ses\n";

	# セッション確認
	} else {

		# クッキー取得
		my $cook = $ENV{HTTP_COOKIE};

		# 該当IDを取り出す
		my %cook;
		foreach ( split(/;/, $cook) ) {
			my ($key,$val) = split(/=/);
			$key =~ s/\s//g;
			$cook{$key} = $val;
		}

		# クッキーなし
		if ($cook{$cf{cookie_id3}} eq '') { pwd_form(); }

		# ログオフのとき
		if ($in{mode} eq 'logoff') {

			my @log;
			open(DAT,"+< $cf{sesfile}") or error("write err: $cf{sesfile}");
			eval 'flock(DAT, 2);';
			while(<DAT>) {
				my ($id,undef) = split(/\t/);
				next if ($cook{$cf{cookie_id3}} eq $id);

				push(@log,$_);
			}
			seek(DAT, 0, 0);
			print DAT @log;
			truncate(DAT, tell(DAT));
			close(DAT);

			if ($ENV{PERLXS} eq "PerlIS") {
				print "HTTP/1.0 302 Temporary Redirection\r\n";
				print "Content-type: text/html\n";
			}
			print "Set-Cookie: $cf{cookie_id3}=;\n";
			print "Location: $cf{homepage}\n\n";
			exit;
		}

		# セッションチェック
		my $flg;
		open(DAT,"$cf{sesfile}") or error("open err: $cf{sesfile}");
		while(<DAT>) {
			chomp;
			my ($id,$time) = split(/\t/);

			if ($cook{$cf{cookie_id3}} eq $id) {
				# 時間オーバー
				if ($now - $time > $cf{sestime} * 60) {
					$flg = -1;
				# OK
				} else {
					$flg = 1;
				}
				last;
			}
		}
		close(DAT);

		# 時間オーバー
		if ($flg == -1) {
			my $msg = qq|入室時間が経過しました。再度ログインしてください<br>\n|;
			$msg .= qq|[<a href="$cf{bbs_cgi}?mode=enter">ログイン</a>]\n|;
			error($msg);

		# セッション情報なし
		} elsif (!$flg) {
			pwd_form();
		}
	}
}

#-----------------------------------------------------------
#  入室画面
#-----------------------------------------------------------
sub pwd_form {
	open(IN,"$cf{tmpldir}/enter.html") or error('open err: enter.html');
	my $tmpl = join('', <IN>);
	close(IN);

	$tmpl =~ s/!bbs_cgi!/$cf{bbs_cgi}/g;

	print "Content-type: text/html; charset=shift_jis\n\n";
	footer($tmpl);
}

#-----------------------------------------------------------
#  画像表記
#-----------------------------------------------------------
sub put_image {
	my ($no,$msg,$ex,$w,$h) = @_;

	my $op;
	if (-f "$cf{upldir}/$no-s$ex") {
		$op = qq|src="$cf{uplurl}/$no-s$ex"|;
	} else {
		my ($w,$h) = resize($w,$h);
		$op = qq|src="$cf{uplurl}/$no$ex" width="$w" height="$h"|;
	}
	$msg .= qq|<p><a href="$cf{uplurl}/$no$ex" target="_blank"><img $op border="0"></a></p>|;
}


1;

